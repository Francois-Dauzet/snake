class Food {
  constructor() {
    this.size = SQUARE_SIZE;
    this.setRandomPosition();
  }

  /// Méthode pour générer aléatoirement le placement de la nourriture
  setRandomPosition() {
    const maxSize = GAME_SIZE / this.size - 1;
    this.x = Math.round(Math.random() * GAME_SIZE) % maxSize;
    this.y = Math.round(Math.random() * GAME_SIZE) % maxSize;
    /// Test des données de placement de la nourriture
    // console.log(this.x, this.y);
  }

  /// Méthode pour afficher la nourriture
  draw() {
    /// Change le style (couleur)
    ctx.fillStyle = "yellow";
    /// Permet de dessiner un rectangle en le redimensionnant
    ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
  }
}
