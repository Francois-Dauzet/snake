class Snake {
  /// Défini la position par defaut
  constructor() {
    this.x = 0;
    this.y = 0;
    this.blockSize = SQUARE_SIZE;
    this.blocks = [];
    this.addBlock(this.x, this.y);
    /// Test création du block
    // console.log(this.blocks);
    /// Test de la création du snake
    // console.log("test_snake_created");
  }
  /// Methode d'ajout de block
  addBlock(x, y) {
    /// Déclare un nouveau block
    const block = new Block(x, y, this.blockSize);
    /// Ajoute le block au snake
    this.blocks.push(block);
  }

  /// Methode pour la direction de la tête
  moveHead() {
    /// Cible la tête du snake
    const head = this.blocks[0];
    /// Test d'incrémentation sur la tête
    // head.x += 1;
    /// Sauvegarde les positions avant leurs mouvement
    head.oldX = head.x;
    head.oldY = head.y;
    /// Switch pour la direction
    switch (currentDirection) {
      case "left":
        head.x -= 1;
        break;
      case "right":
        head.x += 1;
        break;
      case "up":
        head.y -= 1;
        break;
      case "down":
        head.y += 1;
      default:
        break;
    }
    /// Methode pour réinisialiser le snake de part et d'autre du plateau
    head.teleportIfOutMap();
  }

  /// Methode pour calculer la position du dernier block
  calculateNewBlockPosition() {
    /// Parcours les blocks pour trouver le dernier
    /// Version courte
    let { x, y } = this.blocks[this.blocks.length - 1];
    /// Version longue
    // let x = head.x;
    // let y = head.y;
    /// Switch pour choisir le sens d'incrémentation
    switch (currentDirection) {
      case "left":
        x += 1;
        break;
      case "right":
        x -= 1;
        break;
      case "up":
        y += 1;
        break;
      case "down":
        y -= 1;
      default:
        break;
    }
    return { x, y };
  }

  /// Methode pour manger et faire respawn la nourriture
  eat() {
    const head = this.blocks[0];
    if (head.x === food.x && head.y === food.y) {
      food.setRandomPosition();
      /// Donne la position x et y du block
      const { x, y } = this.calculateNewBlockPosition();
      /// Appel la methode pour incrémenter un block
      this.addBlock(x, y);
      score += 1;
    }
  }

  update() {
    /// Methode pour la direction de la tête
    this.moveHead();
    /// Methode pour manger et faire respawn la nourriture
    this.eat();
    /// Boucle sur tous les blocks & récupère l'index
    for (const [index, block] of this.blocks.entries()) {
      if (index > 0) {
        /// Détermine la position du block avant
        const { oldX, oldY } = this.blocks[index - 1];
        /// Test incrémentation nourriture
        // console.log(oldX, oldY, index);
        /// Incrémente la nourriture sur la position
        block.setPosition(oldX, oldY);
      }

      block.draw();
    }
  }
}
