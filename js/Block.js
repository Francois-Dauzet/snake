class Block {
  constructor(x, y, size) {
    this.x = x;
    this.y = y;
    /// Récupère les valeurs précédente des positions
    this.oldX = x;
    this.oldY = y;
    this.size = size;
  }

  /// Methode pour réinisialiser le snake de part et d'autre du plateau
  teleportIfOutMap() {
    const maxSize = GAME_SIZE / this.size;
    /// Gestion de l'axe x
    if (this.x < 0) {
      this.x = maxSize;
    } else if (this.x > maxSize) {
      this.x = 0;
    }
    /// Gestion de l'axe y
    if (this.y < 0) {
      this.y = maxSize;
    } else if (this.y > maxSize) {
      this.y = 0;
    }
  }

  /// Méthode pour définir x et y
  setPosition(x, y) {
    /// Backup des anciennes positions
    this.oldX = this.x;
    this.oldY = this.y;
    /// Défini x & y
    this.x = x;
    this.y = y;
  }

  /// methode d'affichage
  draw() {
    /// Change le style (couleur)
    ctx.fillStyle = "red";
    /// Permet de dessiner un rectangle en le redimensionnant
    ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
  }
}
