/////////////// Constantes ///////////////

/// Taille du plateau
const GAME_SIZE = 600;

/// Taille d'un block
const SQUARE_SIZE = 20;

/// Vitesse du jeu
const GAME_SPEED = 120;

/// Récupere le canvas
const canvas = document.getElementById("game-board");

/// Récupère le contexte du canvas pour ecrire dedans
const ctx = canvas.getContext("2d");

/// Initialisation du snake
const snake = new Snake(SQUARE_SIZE);

/// Déclare la nourriture
const food = new Food();

/////////////// Variables ///////////////

/// Déclare l'écran d'affichage du score
let screenScore = document.querySelector(".screen-score");

/// Score de la partie
let score = 0;

/// Variable pour le choix de la direction
let currentDirection = "";

/////////////// Fonctions ///////////////

/// Fonction pour les touche de direction
function detectKeyPressed() {
  document.addEventListener("keydown", (event) => {
    /// Test keyboard pressed
    // console.log(event.key);
    switch (event.key) {
      case "ArrowLeft":
        currentDirection = "left";
        break;
      case "ArrowRight":
        currentDirection = "right";
        break;
      case "ArrowUp":
        currentDirection = "up";
        break;
      case "ArrowDown":
        currentDirection = "down";
        break;
    }
  });
}

/// Fonction pour clear l'écran
function clearScreen() {
  ctx.clearRect(0, 0, GAME_SIZE, GAME_SIZE);
}

/////////////// Script ///////////////

/// Appeler a chaque frame (image) du jeu
function update() {
  /// Clear l'ecran
  clearScreen();
  /// Affiche la nourriture
  food.draw();
  /// Affiche le block
  snake.update();
  /// Rappel la fonction update en fonction de la vitesse choisi
  setTimeout(update, GAME_SPEED);
  /// Test refresh du jeu
  //   console.log("refresh test");
  screenScore.innerHTML = score;
}

/// Appeler au début du jeu
function start() {
  detectKeyPressed();
  update();
}

start();
